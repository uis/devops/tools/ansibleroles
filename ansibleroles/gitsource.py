"""
Git role source

"""
import dataclasses
import os
import shutil
import tempfile
import typing

import git

from .source import RoleSource


@dataclasses.dataclass
class GitRoleSource(RoleSource):
    """
    Role sources of type "git" have additional fields available.

    """
    #: Repository URL suitable for passing to git clone
    repo: str

    #: Path within repository
    path: str

    #: Ref suitable for passing to --branch option for git clone. If None, use the remote
    #: repository's HEAD.
    ref: typing.Optional[str]

    # Host keys to trust for this repo server
    host_keys: typing.Optional[typing.List[str]]

    def materialise(self, directory):
        # Clone this repository to a temporary directory
        with tempfile.TemporaryDirectory(suffix='-clone') as tmp_dir:
            # Determine where on the filesystem the roles will appear
            path_components = [c for c in self.path.split('/') if c != '']
            roles_path = os.path.join(tmp_dir, *path_components)

            # Construct arguments to git clone
            kwargs = {}
            ssh_cmd = 'ssh'
            if self.ref is not None:
                kwargs['branch'] = self.ref

            tmp_key_file = None
            if self.host_keys:
                tmp_key_file = tempfile.NamedTemporaryFile(suffix='-host-keys', mode='w+')
                for key in self.host_keys:
                    key = key.replace('#', '')
                    key = key.strip()
                    print(key, file=tmp_key_file)
                ssh_cmd += f' -o UserKnownHostsFile="{tmp_key_file.name}"'
                tmp_key_file.seek(0)

            kwargs['env'] = dict(GIT_SSH_COMMAND=ssh_cmd)
            # Shallow clone the repo
            clone = git.Repo.clone_from(self.repo, tmp_dir, depth=1, **kwargs)

            if tmp_key_file is not None:
                tmp_key_file.close()
            # Check the roles directory exists
            if not os.path.isdir(roles_path):
                raise RuntimeError(f'Path "{self.path}" does not exist in repo')

            # Move contents of roles directory into the target directory making sure not to
            # accidentally move the git directory
            for item in os.listdir(roles_path):
                item_path = os.path.join(roles_path, item)

                # Don't copy git dir
                if item_path == clone.git_dir:
                    continue

                shutil.move(item_path, directory)
