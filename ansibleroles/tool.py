"""
Download Ansible roles from online sources

Usage:
    ansibleroles (-h | --help)
    ansibleroles [--quiet] [--configuration=PATH] [--base=DIRECTORY] [--path-only]

Options:

    -h, --help                  Show a brief usage summary.
    --quiet                     Reduce logging verbosity.
    --configuration=PATH        Override location of configuration file (see below).

    -b, --base=DIRECTORY        Specify base directory of role paths.

    --path-only                 Only output colon-separated PATH, not shell configuration.

Operation:

    When run, the role sources specified in the configuration will be fetched to a series of
    directories and the directories will be output as a set of commands suitable for configuring
    bash shells to run ansible-playbook.

    By default the base path is created in the temporary files directory for the system. It can be
    overridden via the --base option.

Configuration:

    Configuration of the tool is via a YAML document. The default location for the configuration is
    the first file which exists in the following locations:

    - The value of the ANSIBLEROLES_CONFIGURATION environment variable
    - ./.ansibleroles.yaml
    - ~/.ansibleroles.yaml
    - /etc/ansibleroles.yaml

    The --configuration option may be used to override the search path.

"""
import logging
import os
import sys
import tempfile

import docopt

from . import config


LOG = logging.getLogger(__name__)


def main():
    opts = docopt.docopt(__doc__)
    logging.basicConfig(level=logging.WARN if opts['--quiet'] else logging.INFO)

    # Load the configuration
    try:
        sources = config.load_configuration(location=opts.get('--configuration'))
    except config.ConfigurationError as e:
        LOG.error('Could not load configuration: %s', e)
        sys.exit(1)

    # Determine which directory the Ansible roles should be materialised in.
    base_dir = opts.get('--base')
    if base_dir is None:
        base_dir = tempfile.mkdtemp(prefix='sources-')

    # Ensure base directory exists
    if not os.path.exists(base_dir):
        os.makedirs(base_dir)

    # Materialise roles.
    try:
        roles_path = ':'.join(materialise_sources(base_dir, sources))
    except Exception as e:
        LOG.error('error: %s', e, exc_info=e)
        sys.exit(1)

    # Generate output.
    if opts['--path-only']:
        print(roles_path)
    else:
        print(f'export ANSIBLE_ROLES_PATH="{roles_path}"')


def materialise_sources(base_dir, sources):
    """
    For each :py:class:`ansibleroles.source.RoleSource` in *sources*, materialise the roles to a
    directory under *base_dir*. Return a the list of materialised directories.

    """
    role_dirs = []
    for source in sources:
        dst_dir = tempfile.mkdtemp(prefix='roles-', dir=base_dir)
        source.materialise(dst_dir)
        role_dirs.append(dst_dir)
    return role_dirs
