"""
Configuration
-------------

.. automodule:: ansibleroles.config
    :members:
    :member-order: bysource

Sources
-------

.. automodule:: ansibleroles.source
    :members:
    :member-order: bysource

.. automodule:: ansibleroles.gitsource
    :members:
    :member-order: bysource

"""
