"""
Generic role source.

"""

import dataclasses


@dataclasses.dataclass
class RoleSource:
    """
    A generic source for a role. Has a name and a type which corresponds to the name and type set
    in the configuration file.

    """
    #: Human-friendly name for the source
    name: str

    #: Type of the source (e.g. "git")
    type: str

    def materialise(self, directory):
        """
        Materialise the roles from this source into the passed directory.

        """
        raise NotImplementedError()
