import os
import shutil
import sys
import tempfile
import unittest

import git

from .. import gitsource


class TestGitMaterialise(unittest.TestCase):
    def setUp(self):
        # Create a testing repository for cloning
        self.repo = create_testing_repo()
        print(f'Test repository: {self.repo.git_dir}', file=sys.stderr)
        self.addCleanup(lambda: shutil.rmtree(self.repo.working_dir))

        # Create a source pointing to the test repo
        self.source = gitsource.GitRoleSource(
            name='Testing', type='git', repo=f'file://{self.repo.git_dir}',
            path='/', ref=None, host_keys={})

        # Create a temporary directory for materialising into
        self.temp_dir = tempfile.mkdtemp()
        print(f'Temporary directory: {self.temp_dir}', file=sys.stderr)
        self.addCleanup(lambda: shutil.rmtree(self.temp_dir))

    def test_basic_functionality(self):
        """The basic materialise function should work."""
        self.source.materialise(self.temp_dir)
        self.assertTrue(os.path.isfile(os.path.join(self.temp_dir, 'role1', 'tasks', 'main.yml')))
        self.assertTrue(os.path.isfile(os.path.join(self.temp_dir, 'role2', 'tasks', 'main.yml')))

    def test_no_git_dir(self):
        """The .git directory should not be present in the roles directory."""
        self.source.materialise(self.temp_dir)
        self.assertFalse(os.path.exists(os.path.join(self.temp_dir, '.git')))

    def test_sub_path(self):
        """Cloning a sub path of the repo should work."""
        subdir_path = os.path.join(self.repo.working_dir, 'subdir')
        os.makedirs(subdir_path)
        self.repo.index.move(['role1', 'subdir/role1'])
        self.repo.index.commit('moved role1')

        self.source.path = '/subdir'
        self.source.materialise(self.temp_dir)

        # Only role1 is present
        self.assertTrue(os.path.isfile(os.path.join(self.temp_dir, 'role1', 'tasks', 'main.yml')))
        self.assertFalse(os.path.exists(os.path.join(self.temp_dir, 'role2')))

    def test_ref(self):
        """Cloning a named ref should work."""
        prev_head = self.repo.head.reference
        self.repo.head.reference = self.repo.create_head('newrole')
        self.repo.index.move(['role1', 'role3'])
        self.repo.index.commit('moved role1 to role3')
        self.repo.head.reference = prev_head

        # role3 is not present in HEAD clone
        d1 = os.path.join(self.temp_dir, 'd1')
        os.makedirs(d1)
        self.source.materialise(d1)
        self.assertFalse(os.path.exists(os.path.join(d1, 'role3')))

        # role3 is present when cloning the newrole ref
        d2 = os.path.join(self.temp_dir, 'd2')
        os.makedirs(d2)
        self.source.ref = 'newrole'
        self.source.materialise(d2)
        self.assertTrue(os.path.exists(os.path.join(d2, 'role3')))

    def test_non_existant_path(self):
        """Attempting to use a sub-path which doesn't exist raises RuntimeError."""
        self.source.path = '/does/not/exist'
        with self.assertRaises(RuntimeError):
            self.source.materialise(self.temp_dir)


def create_testing_repo():
    """
    Creates a temporary testing bare repository and returns the Repo object. It is the
    responsibility of the caller to clean up the repo afterwards.

    The repository initially has a single commit on master creating two roles: role1 and role2.

    """
    repo_dir = tempfile.mkdtemp(suffix='-testing.git')
    repo = git.Repo.init(repo_dir)

    r1_dir = os.path.join(repo.working_dir, 'role1')
    os.makedirs(os.path.join(r1_dir, 'tasks'))

    r2_dir = os.path.join(repo.working_dir, 'role2')
    os.makedirs(os.path.join(r2_dir, 'tasks'))

    with open(os.path.join(r1_dir, 'tasks', 'main.yml'), 'w') as fobj:
        fobj.write('''
- name: testing
debug:
message: foo
''')

    with open(os.path.join(r2_dir, 'tasks', 'main.yml'), 'w') as fobj:
        fobj.write('''
- name: testing
debug:
message: bar
''')

    repo.index.add([r1_dir, r2_dir])
    repo.index.commit('initial commit')

    return repo
