import unittest

from ..source import RoleSource


class TestRoleSource(unittest.TestCase):
    def test_materialise_not_implemented(self):
        source = RoleSource(name='foo', type='foo')
        with self.assertRaises(NotImplementedError):
            source.materialise('/some/dir')
