import unittest

from .. import config


class TestGenericConfig(unittest.TestCase):
    def test_config_must_be_sequence(self):
        """The configuration must be a sequence."""
        with self.assertRaises(config.ConfigurationParseError):
            config._load_config('''
git:
  repo: git@github.com:example/invalid.git
''')

    def test_role_source_must_be_dict(self):
        """A role source must be a dictionary."""
        with self.assertRaises(config.ConfigurationParseError):
            config._load_config('''
- ['git']
''')

    def test_unknown_role_type(self):
        """An unknown role type raises an error"""
        with self.assertRaises(config.ConfigurationParseError):
            config._load_config('''
- name: some role
  from_my_imagination:
    source: brain
''')


class TestGitRole(unittest.TestCase):
    def test_short_config(self):
        """Minimal configuration parses."""
        c = config._load_config('''
- git:
    repo: git@github.com:example/invalid.git
''')

        self.assertEqual(len(c), 1)
        source = c[0]
        self.assertEqual(source.type, 'git')
        self.assertIsInstance(source.name, str)
        self.assertNotEqual(source.name, '')
        self.assertEqual(source.repo, 'git@github.com:example/invalid.git')
        self.assertEqual(source.path, '/')
        self.assertIsNone(source.ref)

    def test_full_config(self):
        """Full configuration parses."""
        c = config._load_config('''
- name: 'Some name'
  git:
    repo: git@github.com:example/invalid.git
    path: /foo
    ref: prod
''')

        self.assertEqual(len(c), 1)
        source = c[0]
        self.assertEqual(source.type, 'git')
        self.assertEqual(source.name, 'Some name')
        self.assertEqual(source.repo, 'git@github.com:example/invalid.git')
        self.assertEqual(source.path, '/foo')
        self.assertEqual(source.ref, 'prod')

    def test_absolute_path(self):
        """The path must be absolute."""
        with self.assertRaises(config.ConfigurationParseError):
            config._load_config('''
- git:
    repo: git@github.com:example/invalid.git
    path: ../foo
''')

    def test_repo_required(self):
        """The repo must be specified."""
        with self.assertRaises(config.ConfigurationParseError):
            config._load_config('''
- git:
    path: /
''')
