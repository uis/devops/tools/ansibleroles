"""
Parse and load configuration.

"""

import collections
import collections.abc
import logging
import os

import yaml

from .gitsource import GitRoleSource

LOG = logging.getLogger(__name__)


class ConfigurationError(RuntimeError):
    """
    Base class for all errors in loading configuration.

    """


class ConfigurationParseError(ConfigurationError):
    """
    Indicates a problem with the format of the configuration file.

    """


class ConfigurationNotFound(ConfigurationError):
    """
    Indicates that the configuration file could not be found in the location search path.

    """

    def __init__(self):
        return super().__init__('Could not find any configuration file')


def load_configuration(location=None):
    """
    Load configuration and return a sequence of :py:class:`ansibleroles.source.RoleSource`
    instances. Pass a non-None location to override the default search path.

    :raises: ConfigurationError if the configuration could not be loaded.

    """
    if location is not None:
        paths = [location]
    else:
        if 'ANSIBLEROLES_CONFIGURATION' in os.environ:
            paths = [os.environ['ANSIBLEROLES_CONFIGURATION']]
        else:
            paths = []
        paths.extend([
            os.path.join(os.getcwd(), '.ansibleroles.yaml'),
            os.path.expanduser('~/.ansibleroles.yaml'),
            '/etc/ansibleroles.yaml'
        ])

    valid_paths = [path for path in paths if os.path.isfile(path)]

    if len(valid_paths) == 0:
        LOG.error('Could not find configuration file. Tried:')
        for path in paths:
            LOG.error('"%s"', path)
        raise ConfigurationNotFound()

    with open(valid_paths[0]) as fobj:
        return _load_config(fobj)


def _load_config(fobj_or_string):
    """
    Load configuration from either a file-like object or a string.

    Returns a sequence of :py:class:`RoleSource` sub-classes.

    """
    seq = yaml.safe_load(fobj_or_string)
    if not isinstance(seq, collections.abc.Sequence):
        raise ConfigurationParseError(
            'Role source collection must be sequence of source definitions')

    return [_load_source(d) for d in seq]


def _load_source(source_dict):
    if not isinstance(source_dict, collections.abc.Mapping):
        raise ConfigurationParseError('Role source must be a dictionary')

    if 'git' in source_dict:
        rv = _load_git_source(source_dict['git'])
    else:
        raise ConfigurationParseError('Cannot determine role type')

    # Override name field if set
    if 'name' in source_dict:
        rv.name = source_dict['name']

    return rv


def _load_git_source(source_dict):
    repo = source_dict.get('repo')
    if repo is None:
        raise ConfigurationParseError('repo key is required for git sources')

    path = source_dict.get('path', '/')
    if os.path.abspath(path) != path:
        raise ConfigurationParseError('path must be an absolute path')

    return GitRoleSource(
        name=f'Git repository at {repo}',
        type='git',
        repo=repo,
        path=path,
        ref=source_dict.get('ref'),
        host_keys=source_dict.get('host_keys')
    )
