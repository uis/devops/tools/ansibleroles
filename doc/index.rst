Ansible Role Downloader
=======================

The Ansible Role Downloader is a command line utility intended to be used in
scripts which prepare environments for running Ansible playbooks. It parses a
configuration file specifying sources for roles, downloads them and outputs
configuration suitable for configuring the local shell.

.. toctree::
    :maxdepth: 2
    :caption: Contents

    reference
