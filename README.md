# Ansible Role Downloader

The Ansible Role Downloader is a command line utility intended to be used in
scripts which prepare environments for running Ansible playbooks. It parses a
configuration file specifying sources for roles, download roles from those
sources into a set of directories and outputs configuration suitable for
configuring the local shell.

## Installation

```bash
$ pip install git+https://gitlab.developers.cam.ac.uk/uis/devops/tools/ansibleroles.git
```

## Example usage

Create a ``.ansibleroles.yaml`` file in the current directory with the following
content:

```yaml
- git:
    repo: https://gitlab.developers.cam.ac.uk/uis/devops/infra/ansible-roles.git
    path: /roles
```

To configure ``ansible-playbook`` to use these shared roles:

```bash
$ eval $(ansibleroles)
$ ansible-playbook # ... remainder of command
```

To copy the roles to a specified base directory rather than creating a new
temporary directory each time:

```bash
$ rm -rf roles/  # delete any previous download
$ eval $(ansibleroles -b roles/)
$ ansible-playbook # ... remainder of command
```

## Development

In development we build the utility inside a container. To run the utility and
download roles to a temporary output directory:

```bash
$ OUT_DIR=$(mktemp -d -p build/ roles-XXXXX)
$ COMPOSE_ARGS="-v $OUT_DIR:/output" ./ansibleroles_development.sh \
  --base=/output --configuration=ansibleroles.example.yaml
```

## Further documentation

Usage and API documentation may be built using tox:

```bash
$ COMPOSE_ARGS="-v $PWD/build/:/tmp/tox-data/artefacts/" ./tox.sh -e doc
$ xdg-open build/doc/index.html   # Linux
$ open build/doc/index.html       # Mac
```
