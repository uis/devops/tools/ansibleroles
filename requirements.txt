GitPython
PyYAML
docopt

# The dataclasses module is in the standard library for Python >= 3.7
dataclasses; python_version < '3.7'

# Required to run the tests. So that we may test the production image, tox is
# included in production image builds.
tox
