#!/usr/bin/env bash
#
# Wrapper script to run tox. Arguments are passed directly to tox.
#
# The COMPOSE_ARGS environment variable may be used to pass additional arguments
# to docker-compose.
#
# If DISABLE_BIND_MOUNT is set, the repository root will *not* be added as a
# read-only bind mount. This is mostly useful if you want to test the production
# image "as is".

# Exit on failure
set -e

# Change to this script's directory
cd "$( dirname "${BASH_SOURCE[0]}")"

if [ -z "${DISABLE_BIND_MOUNT}" ]; then
    VOLUME_ARGS="-v $PWD:/usr/src/app:ro"
else
    VOLUME_ARGS=
fi

# Execute tox runner, logging command used
set -x
exec ./compose.sh tox run --rm $VOLUME_ARGS $COMPOSE_ARGS tox $@
